package rh.demo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class MeterList implements Serializable{

    private static final long serialVersionUID = 1L;
    
    @XmlElement(name="meter")
    private List<Meter> meters = new ArrayList<>();

    public MeterList(){}

    public MeterList(List<Meter> meters){
        this.meters = meters;
    }

    public static class Meter {
        @XmlElement
        private String name;

        public Meter(){}

        public Meter(String name){
            this.name = name;
        }
    }

    
}