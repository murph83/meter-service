package rh.demo;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;

import org.apache.camel.component.cxf.CxfEndpoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "ws")
public class WebServiceConfig {

    Map<String, WSConfiguration> config = new HashMap<>();

    @Autowired
    ConfigurableApplicationContext applicationContext;

    @PostConstruct
    public void initServices() throws ClassNotFoundException{
        ConfigurableListableBeanFactory beanFactory = applicationContext.getBeanFactory();

        for (Entry<String, WSConfiguration> service : config.entrySet()) {
            CxfEndpoint bean = new CxfEndpoint();
            bean.setAddress(service.getValue().address);
            bean.setServiceClass(Class.forName(service.getValue().serviceClass));
            
            beanFactory.autowireBean(bean);
            beanFactory.initializeBean(bean, service.getKey());
            beanFactory.registerSingleton(service.getKey(), bean);
        }

    }

    public static class WSConfiguration {
        String address;
        String serviceClass;

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getServiceClass() {
            return serviceClass;
        }

        public void setServiceClass(String serviceClass) {
            this.serviceClass = serviceClass;
        }

    }

    public Map<String, WSConfiguration> getConfig() {
        return config;
    }

}