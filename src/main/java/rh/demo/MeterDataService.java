package rh.demo;

import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.WebService;

@WebService(
    name = "MeterDataService",
    serviceName = "MeterDataService",    
    portName = "SOAPOverHTTP",
    targetNamespace = "http://rh.demo/integration/services/MeterDataService/"
)
public interface MeterDataService{

    @WebMethod(action = "http://rh.demo/integration/services/MeterDataService/listMeters")
    @WebResult(name = "meterList")
    MeterList listMeters();

}