package rh.demo;

import java.util.Arrays;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class MeterServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(MeterServiceApplication.class, args);
    }

    @Bean
    public RouteBuilder routes(){
        return new RouteBuilder(){
        
            @Override
            public void configure() throws Exception {
                from("cxf:bean:meterDataService")
                    .to("log:meterDataService")
                    .setBody(constant(new MeterList(Arrays.asList(new MeterList.Meter("A Meter")))));
               
            }
        };
    }

}